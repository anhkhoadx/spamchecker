module backend

go 1.14

require (
	github.com/aws/aws-lambda-go v1.15.0
	github.com/gin-gonic/gin v1.6.1
)
