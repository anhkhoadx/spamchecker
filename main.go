package main

import (
	"fmt"
	"log"
	"os"

	"github.com/aws/aws-lambda-go/lambda"
)

func HandleRequest() {
	log.Print("Event received.")
}

func main() {
	l := log.New(os.Stderr, "", 1)
	l.Println("log message")
	fmt.Println("Common")
	lambda.Start(HandleRequest)
}
